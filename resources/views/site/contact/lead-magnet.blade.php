@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/formvalidation/dist/css/formValidation.css') }}">
@endsection

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row justify-content-center align-items-center">                                
        <div class="col-lg-12 blog-main">

          <div class="blog-post">                 
               
                <h2>PLUS 6 Direct Marketing Tips to Improve Your Conversion Rate</h2>
                
                <p>The connection you build with your audience can spell the difference between your campaign’s failure or success. It is key for organisations to identify the most effective means to establish this connection, especially for businesses that offer highly-specialized products and services. That is why Direct
					Marketing is the choice of many marketers to deliver business results at a great cost.</p>

				<p>Considering Direct Marketing for your business? Download our handy guide to find out how to design a successful direct marketing campaign.</p>

                <form id="lead-magnet-form" method="post" action="{{ url('steps-to-supercharge-your-direct-marketing/save-message') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">                   

                    <input type="textbox" name="name" value="" placeholder="Name" required>
                    <input type="textbox" name="email" value="" placeholder="Email" required>
                        
                    
                    <div class="form-row">
                        <div class="col-12 col-sm-10 g-recaptcha-container">
                            <div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
                            @if ($errors->has('g-recaptcha-response'))
                                <div class="fv-help-block">{{ $errors->first('g-recaptcha-response') }}</div>
                            @endif
                        </div>
                    </div>
                    <button type="submit" class="btn-submit">Submit</button>
                </form>                      
        </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->    
@endsection
@section('scripts')
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

