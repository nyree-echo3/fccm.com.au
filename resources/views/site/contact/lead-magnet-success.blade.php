@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead">         
    <div class="container">

      <div class="row justify-content-center align-items-center">              
        <!-- @include('site/partials/sidebar-contact')  -->      
                     
        <div class="col-lg-12 blog-main">

          <div class="blog-post">     
               <p>Thank you for your submission. We will receive an email shortly with your download guide.</p>
               
               <div class="contact-details">           				          
				   {!! $contact_details !!}
				   <div class="contact-map">
					  {!! $contact_map !!}
				   </div>
			   </div>                                  
            </div>
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->    
@endsection            
