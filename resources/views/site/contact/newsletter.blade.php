@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/formvalidation/dist/css/formValidation.css') }}">
@endsection

@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')

    <div class="blog-masthead ">
        <div class="container">

            <div class="row justify-content-center align-items-center">
                <div class="col-lg-12 blog-main">

                    <div class="blog-post">

                        <h2>Keep up-to-date!</h2>
                        <p>Sign up below for our latest news, industry updates and solutions for clients.</p>

                        <form id="newsletter-form" method="post" action="{{ url('newsletter/save-message') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <input type="textbox" name="name" value="" placeholder="First Name" required>
                            <input type="textbox" name="last-name" value="" placeholder="Last Name" required>
                            <input type="textbox" name="company-name" value="" placeholder="Company Name" required>
                            <input type="textbox" name="contact-number" value="" placeholder="Contact Number" required>
                            <input type="textbox" name="email" value="" placeholder="Email" required>

                            <div class="form-row">
                                <div class="col-12 col-sm-10 g-recaptcha-container">
                                    <div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
                                    @if ($errors->has('g-recaptcha-response'))
                                        <div class="fv-help-block">{{ $errors->first('g-recaptcha-response') }}</div>
                                    @endif
                                </div>
                            </div>
                            <button type="submit" class="btn-submit">Submit</button>
                        </form>
                    </div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->
@endsection
@section('scripts')
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection
