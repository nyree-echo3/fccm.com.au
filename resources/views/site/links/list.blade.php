<?php 
   // Set Meta Tags
   $meta_title_inner = ($category_name == "Latest Links" ? $category_name : $category_name . " - Links"); 
   $meta_keywords_inner = "Links"; 
   $meta_description_inner = ($category_name == "Latest Links" ? $category_name : $category_name . " - Links");  
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead">         
    <div class="container">

      <div class="row">
        @include('site/partials/sidebar-links')
        
        <div class="col-lg-9 blog-main px-5">

                                                          	            
	              
            @if(isset($items))                            
                     <div class="blog-post row m-0">                                                                               
                          @php
						     $counter=0;
						  @endphp       
						        
						  @foreach($items as $item)   
						        @php
						           $counter++;
						        @endphp    
						        
				                @if ($item->image != "")	
									<div class="col-sm-3 panel-links-item panel-links-item-1">									             
										<div class="div-img">
										   <img src="{{ url('') }}/{{ $item->image }}" alt="{{$item->name}}" />	
										</div>																		
									</div>
                                @endif
                                
								<div class="{{ ($item->image != "" ? "col-sm-9 panel-links-item panel-links-item-2" : "col-sm-12 panel-links-item panel-links-item-2") }}" >								  
								     <span id="divLink{{ $counter }}">&nbsp;</span>
									 <h2>{{$item->name}}</h2>									 																								
									 								
									 <div class="panel-news-item-shortdesc">{!! $item->description !!}</div>																 
								</div>                    								                														                        								              								
																													                                                    
						   @endforeach                                   
			  		</div>                                          
               @else
                 <p>Currently there is no information to display.</p>    
               @endif
          
   
         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->
@endsection
