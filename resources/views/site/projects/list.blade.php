<?php 
   // Set Meta Tags
   $meta_title_inner = ($category_name == "Latest Projects" ? $category_name : $category_name . " - Projects"); 
   $meta_keywords_inner = "Projects"; 
   $meta_description_inner = ($category_name == "Latest Projects" ? $category_name : $category_name . " - Projects");  
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @include('site/partials/sidebar-projects')
        
        <div class="col-lg-9 blog-main px-5">

          <div class="blog-post">                                                   	            
	              
            @if(isset($items))          
                  <section class="project-block cards-project">
                     <div class="container">	  
                                                                       
					  @foreach($items as $item) 
				        <div class="projects-list-box">    
							<a href='{{ url('').'/'.$item->url }}'>                      								
								<div class='project-list-item'>
									<div class='project-list-item-txt'>					  
										<h2 class="blog-post-title">{{$item->title}}</h2>
										{!! $item["short_description"] !!}
										<div class="readmore">more ></div>
									</div>

									@if (count($item->images) > 0)	
										<div class="card border-0">	
											<div class='project-list-item-img'>										
												<img src="{{ url('') }}{{$item->images[0]->location}}" alt="{{$item->images[0]->name}}" class="card-img-top">										
											</div>
										</div>	
									@endif
								</div>
							</a>
						</div>																							                                                    
					   @endforeach                                   
	            
                   </section>  
                   <!-- Pagination -->
                   <div id="pagination">{{ $items->links() }}</div>
              
               @else
                 <p>Currently there is no projects to display.</p>    
               @endif
          
   
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div>
@endsection
