<?php 
   // Set Meta Tags
   $meta_title_inner = $page->meta_title; 
   $meta_keywords_inner = $page->meta_keywords; 
   $meta_description_inner = $page->meta_description;  
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">        
        @include('site/partials/sidebar-pages')                
        
        <div class="col-lg-9 blog-main px-5">

          <div class="blog-post">
            @if (isset($page))            
            {!! $page->body !!}
            @endif
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead --> 

@if (isset($page) && $page->popup_type != "")    
    @include('site/partials/popup') 
@endif              

@endsection
