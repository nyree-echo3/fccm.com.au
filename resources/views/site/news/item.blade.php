<?php
// Set Meta Tags
$meta_title_inner = $news_item->meta_title;
$meta_keywords_inner = $news_item->meta_keywords;
$meta_description_inner = $news_item->meta_description;
?>
@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')


    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                @include('site/partials/sidebar-news')

                <div class="col-lg-9 blog-main px-5">

                    <div class="blog-post">

                        <div class='news-item'>

                            @if($news_item->thumbnail != "")
                                <img src='{{ url('') }}/{{ $news_item->thumbnail }}' align="right" alt='{{ $news_item->title }}'>
                            @endif
                                {!! $news_item->body !!}
                        </div>

                        <div class='btn-back'>
                            <a class='btn-back' href='{{ url('') }}/news/{{ $news_item->category->slug }}'><i class='fa fa-chevron-left'></i> back</a>
                        </div>

                        @include('site/partials/helper-sharing')
                    </div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->

    @if (isset($news_item) && $news_item->popup_type != "")
        @include('site/partials/popup')
    @endif

@endsection
