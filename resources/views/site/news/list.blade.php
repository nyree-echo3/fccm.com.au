<?php 
   // Set Meta Tags
   $meta_title_inner = ($category_name == "Latest News" ? $category_name : $category_name . " - News"); 
   $meta_keywords_inner = "News"; 
   $meta_description_inner = ($category_name == "Latest News" ? $category_name : $category_name . " - News");  
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @include('site/partials/sidebar-news')
        
        <div class="col-lg-9 blog-main px-5">

          <div class="blog-post">           
            
            @if(isset($items)) 
                  <div class="card-columns">
                                                                                 
                  @foreach($items as $item)                 
					  <div class="card">										
							<div class="card-body">
							<div class="panel-news-item">	
								<a  class="projects-more"  href="{{ url('') }}/news/{{ $item->category->slug }}/{{$item->slug}}">
									@if ($item->thumbnail != "")											   
										<div class="div-img">
										   <img src="{{ url('') }}/{{ $item->thumbnail }}" alt="{{$item->title}}" />	
										</div>			
									@endif	                                    

									<div class="panel-news-item-title"><h2>{{$item->title}}</h2></div>
									<div class="panel-news-item-shortdesc">{!! $item->short_description !!}</div>

									<div class="panel-news-item-readmore">Read More ></div>													                                               
								</a>
								  </div>  
							</div>



						</div>                                                   
                   @endforeach
			       </div>
                   
                   <!-- Pagination -->
                   <div id="pagination">{{ $items->links() }}</div>
              
               @else
                 <p>Currently there is no news items to display.</p>    
               @endif
          
   
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div>
@endsection
