<?php 
   // Set Meta Tags
   $meta_title_inner = "Archived News"; 
   $meta_keywords_inner = "Archived News"; 
   $meta_description_inner = "Archived News";  
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead">         
    <div class="container">

      <div class="row">
        @include('site/partials/sidebar-news-archive')
        
        <div class="col-lg-9 blog-main px-5">

          <div class="blog-post">           
            
            @if(isset($items))                                                                
                  @foreach($items as $news_item)                 
					  <div class='news-list-item'>					  
						<h2 class="blog-post-title">{{$news_item->title}}</h2>
						{!! $news_item["short_description"] !!}
						<a class='btn-submit' href='{{ url('') }}/news/{{ $news_item->category->slug }}/{{$news_item->slug}}'>more</a>
					  </div>                                                          
                   @endforeach
                   
                   <!-- Pagination -->
                   <div id="pagination">{{ $items->links() }}</div>
              
               @else
                 <p>Currently there is no news items to display.</p>    
               @endif
          
   
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->    
@endsection
