@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel')
@include('site/partials/index-panel-seo')
@include('site/partials/index-panel-partners')
@include('site/partials/index-panel-newsletter')
@include('site/partials/index-panel-contact')

@endsection
