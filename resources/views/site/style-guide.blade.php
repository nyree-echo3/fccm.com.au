@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead">         
    <div class="container">

      <div class="row justify-content-center align-items-center">
        <!--
        <div class="col-sm-3 offset-sm-1 blog-sidebar">         
		  <div class="sidebar-module">
			<h4>Style Guide</h4>       
		  </div>
		</div>
        -->
       
        <div class="col-lg-10 blog-main">

          <div class="blog-post">   
            <h2>Heading 2</h2>
                               
            <blockquote>
			<p>Blockquote Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
			</blockquote>                                                   			

			<ul>
				<li>List Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</li>
				<li>List Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</li>
			</ul>
			
			<p>P Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>			
			
			<hr />			

			<h3>Heading 3 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</h3>
           
            <h4>Heading 4 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</h4>
            
            <h5>Heading 5 Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</h5>
            
            <a class="cta" href="{{ url('') }}">CTA eg. Download Now: 6 Free Templates</a>

              <div class="code-block">
                  <h2>HTML</h2>
                  {{ '<a class="cta" href="'}}{{ url('') }}{{'">CTA eg. Download Now: 6 Free Templates</a>' }}
              </div>

            <div class="lead-magnet">
                  <div class="lm-img"><img src="{{ url('') }}/images/site/pop-up-12.png" alt="Twelve" text="Twelve"></div>
                  <div class="lm-txt">
                      <div class="lm-h1">Steps to <div>Supercharge</div> Your Direct Marketing</div>
                      <div class="lm-h2"><div>PLUS</div> 6 Direct Marketing Tips to Improve Your Conversion Rate</div>
                      <a href="{{ url('') }}/steps-to-supercharge-your-direct-marketing" class="lm-a">Download our free booklet <i class="fas fa-arrow-right"></i></a>
                  </div>
            </div>

              <div class="code-block">
                  <h2>HTML</h2>
                  {{ '<div class="lead-magnet">' }}<br>
                      {{ '<div class="lm-img"><img src="'}}{{ url('') }}{{'/images/site/pop-up-12.png" alt="Twelve" text="Twelve"></div>' }}<br>
                            {{ '<div class="lm-txt">' }}<br>
                                {{ '<div class="lm-h1">Steps to <div>Supercharge</div> Your Direct Marketing</div>' }}<br>
                                {{ '<div class="lm-h2"><div>PLUS</div> 6 Direct Marketing Tips to Improve Your Conversion Rate</div>' }}<br>
                                {{ '<a href="'}}{{ url('') }}{{'/steps-to-supercharge-your-direct-marketing" class="lm-a">Download our free booklet <i class="fas fa-arrow-right"></i></a>' }}<br>
                            {{ '</div>' }}<br>
                      {{ '</div>' }}<br>
                  {{ '</div>' }}<br>

</div><!-- /.blog-post -->
</div><!-- /.blog-main -->

</div><!-- /.row -->

</div><!-- /.container -->
</div><!-- /.blog-masthead -->

@endsection
