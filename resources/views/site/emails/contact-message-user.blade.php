<!DOCTYPE html>
<html>
<head>
    <style>
        h1 { color: #0c1525 ; font-weight: bold; font-size: 18px; text-transform: uppercase; line-height: 120%; padding: 15px 0 5px 0;}
        h2 { color: #0c1525 ; font-weight: bold; font-size: 16px; line-height: 120%; padding: 15px 0 5px 0; border-bottom: 1px #891637 solid;}
    </style>
</head>
<body>
<img src="{{ url('') }}/images/site/email-logo.png">
<p>Hi,</p>

@if ($contact_message->type == "Contact Form")
<p>Thank you for your enquiry. We will be in touch with you shortly.</p>
@endif

@if ($contact_message->type == "Newsletter")
    <p>Thank you for subscribing to our newsletters.</p>
@endif

@if ($contact_message->type == "Lead Magnet")
    <p>Thank you for your submission.  Please find attached your Direct Marketing download guide.</p>
@endif

<p>Kind regards,<br>
<img src="{{ url('') }}/images/site/email-thanks.png">
</body>
</html>