<footer class='footer'>     
  <div class='footerContainer'>
      <div id="footer-menu">                 
         <div class="container-fluid">
		    <div class="row justify-content-center align-items-center">         			 
		    
			    <div class="col-lg-3">			  
				   <a href="{{ url('') }}/index" title="{{ $company_name }}"><img class="footer-logo" src="{{ url('') }}/images/site/logo.gif" title="{{ $company_name }}" alt="{{ $company_name }}"></a>
			    </div>
			    
			    <div class="col-lg-3">	
			       <div class="footer-address">
					   <h2>Where to find us</h2>
					   {!! $address !!}
				   </div>		  
			    </div>
			    
			    <div class="col-lg-3">	
			       <div class="footer-contact">		  
				   <h2>Contact</h2>
				   @if ( $phone_number != "") <div>T <a href="tel:{{ str_replace(' ', '', $phone_number) }}">{{ $phone_number }}</a></div> @endif
				   @if ( $fax_number != "") <div>F <span>{{ $fax_number }}</span></div> @endif 
				   @if ( $email != "") <div>E <a href="mailto:{{ $email }}">{{ $email }}</a></div> @endif 
				   </div>
			    </div>
			    
			    <div class="col-lg-3">			  
				   <a href="{{ url('') }}/index" title="{{ $company_name }}"><img class="footer-review" src="{{ url('') }}/images/site/logo-footer-ao.png" title="Australian Owned" alt="Australian Owned"></a>
			    </div>
			    			   		    			  			   			  			   
		     </div>			 
	      </div>	      	    
      </div>	   
 
	  <div id="footer-txt"> 
		  <div><a href="{{ url('') }}">&copy; {{ date('Y') }} FCCM</a><span> |</span></div>
		  <div><a href="{{ url('') }}/contact">Contact</a><span> |</span></div>  
		  <div><a href="{{ url('') }}/pages/other/terms--conditions">Terms & Conditions</a><span> |</span></div> 
		  <!--<div><a href="{{ url('') }}/pages/other/shipping--returns">Shipping & Returns</a><span> |</span></div>-->
		  <div><a href="{{ url('') }}/pages/other/privacy-policy">Privacy Policy</a><span> |</span></div> 
		  <div><a href="https://www.echo3.com.au" target="_blank">Website by Echo3</a></div>     
	  </div>
	  
  </div>
</footer>