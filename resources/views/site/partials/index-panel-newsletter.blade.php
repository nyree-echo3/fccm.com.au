<div data-aos="fade-up" data-aos-duration="2000">
	<div class="index-panel-newsletter">
	   <div class="container-fluid">
		  <div class="row no-gutters h-100 align-items-center">   
		      <div class="col-lg-6">			 
				  <div class="index-panel-newsletter-img">
					 <img src="{{ url('') }}/images/site/pic3.jpg" alt="Newsletter" >
				  </div>						  			 
			  </div><!-- /.col-lg-6 -->		
			        			 
			  <div class="col-lg-6">
				  <div class="index-panel-newsletter-txt">			 
					  <h2>Keep up-to-date!</h2>
					  <p>At First Class Consulting & Mailing, our customers are at the heart of our approach. By bringing together cutting-edge technology and industry expertise, we provide the in-depth insight that our customers demand to deliver the best return on your investment.</p>
					  <p>Our services include call centre facilities and servicing, secure payment gateway and order processing, administration solutions, mail house and fulfilment, warehousing including pharmaceutical storage and distribution and much more.</p>
					  <p>If you would like to find out more or keep up-to-date with our latest news, industry updates and solutions for clients, be sure to sign up for our newsletter here. </p>
					  <div class="index-panel-newsletter-btn"><a class="btn-submit" href="{{ url('') }}/newsletter">Subscribe to our news</a></div>
				  </div>
			  </div><!-- /.col-lg-6 -->				  		 	

			</div><!-- /.row -->	
	   </div><!-- /.container -->	
	</div><!-- /.index-panel-seo -->
</div>	

<div class="index-panel-newsletter-img-resp">
    <img src="{{ url('') }}/images/site/pic3.jpg" alt="Newsletter">
</div>	