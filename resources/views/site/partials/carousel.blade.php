<div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
    <div class="carousel-inner">

        <?php $counter = 0; ?>
        @foreach($images as $image)
            <?php $counter++;  ?>

            <div class="carousel-item {{ $counter == 1 ? ' active' : '' }}">
                <img class="slide" src="{{ url('') }}/{{ $image->location }}" alt="{{ $image->title }}">
                <div class="container">

                    <div class="carousel-caption">
                        <div class="carousel-caption-div">
                            @if ( $image->title != "")<div class="carousel-caption-div-header">{!! $image->title !!}</div> @endif
                            @if ( $image->description != "") <p>{{ $image->description }}</p> @endif
                            @if ( $image->url != "")<p><a class="btn btn-lg btn-primary" href="{{ $image->url }}" role="button">Click for more</a></p> @endif
                        </div>
                    </div>

                    <div class="carousel-tabs">

                        <a class="carousel-tab-a1" href='{{ url('pages/services') }}'>
                            <div class="carousel-tab carousel-tab1">
                                <div>></div>
                                <div>DIRECT MARKETING</div>
                                <div>Engage customers with promotional packages</div>
                            </div>
                        </a>

                        <a class="carousel-tab-a2" href='{{ url('pages/services') }}'>
                            <div class="carousel-tab carousel-tab2">
                                <div>></div>
                                <div>CUSTOMER CARE</div>
                                <div>Call centre, membership services & mail-outs</div>
                            </div>
                        </a>

                    </div>

                </div>
            </div>
        @endforeach

    </div>
</div>