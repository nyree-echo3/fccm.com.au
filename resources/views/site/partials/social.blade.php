<div class="social-tab {{ (url('') != Request::url() && url('') . "/index" != Request::url() ? "social-tab-inside" : "") }}">
	 @if ( $social_facebook != "") <div><a href="{{ $social_facebook }}" target="_blank"><i class='fab fa-facebook-f'></i></a></div> @endif
	 @if ( $social_linkedin != "") <div><a href="{{ $social_linkedin }}" target="_blank"><i class='fab fa-linkedin-in'></i></a></div> @endif
	 @if ( $social_twitter != "") <div><a href="{{ $social_twitter }}" target="_blank"><i class='fab fa-twitter'></i></a></div> @endif 
	 @if ( $social_instagram != "") <div><a href="{{ $social_instagram }}" target="_blank"><i class="fab fa-instagram"></i></a></div> @endif	 	 
	 @if ( $social_googleplus != "") <div><a href="{{ $social_googleplus }}" target="_blank"><i class='fab fa-google-plus-g'></i></a></div> @endif	 
	 @if ( $social_pinterest != "") <div><a href="{{ $social_pinterest }}" target="_blank"><i class='fab fa-pinterest-p'></i></a></div> @endif
	 @if ( $social_youtube != "") <div><a href="{{ $social_youtube }}" target="_blank"><i class='fab fa-youtube'></i></a></div> @endif	
</div>