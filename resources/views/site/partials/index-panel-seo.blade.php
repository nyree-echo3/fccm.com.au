<div data-aos="fade-up" data-aos-duration="2000">
	<div class="index-panel-seo">
	   <div class="container-fluid">
		  <div class="row no-gutters h-100 align-items-center">         			 

			  <div class="col-lg-6">			 
				  <div class="index-panel-seo-img">
					 <img src="{{ url('') }}/images/site/pic1.jpg" alt="Why we're different" >
				  </div>						  			 
			  </div><!-- /.col-lg-6 -->		

			 <div class="col-lg-6">
				  <div class="index-panel-seo-txt">			 
					  {!! $home_intro_text !!}				 
				  </div>
			  </div><!-- /.col-lg-6 -->		

			</div><!-- /.row -->	
	   </div><!-- /.container -->	
	</div><!-- /.index-panel-seo -->	
</div>