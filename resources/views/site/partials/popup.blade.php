@section('styles')
    <link rel="stylesheet" href="{{ asset('css/site/popup.css?v0.4') }}">
@endsection
   
<!-- Modal - Popup -->
@php
   $popup_position = "";
   $popup_start = 0;
   $popup_end = 0;
   
   if (isset($page)) {
      $popup_position = $page->popup_position;
      $popup_start = $page->popup_start;
      $popup_end = $page->popup_end;
   }
   
   if (isset($news_item)) {
      $popup_position = $news_item->popup_position;
      $popup_start = $news_item->popup_start;
      $popup_end = $news_item->popup_end;
   }
   
   switch ($popup_position)  {
      case "top-left":
      case "bottom-left": 
         $slideout = "left";
         break;
         
      default:
         $slideout = "right";
         break;   
   }
@endphp

<div class="modal fade modal-{{ $popup_position }}" id="modal-popup" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="false" data-focus="false">  
  <div class="modal-dialog modal-dialog-slideout modal-dialog-slideout-{{ $slideout }} modal-md" role="document">        
    <div class="modal-content">     
      <div class="modal-body">
        <div class="modal-btn">
		    <a class="" href='#' data-dismiss="modal"><img src="{{ url('') }}/images/site/close.png" title="Close" alt="Close"></a>
		 </div>	

        {!! $popup->form !!}
       
      </div>      
    </div>     
  </div>

</div>


@section('inline-scripts-popups')
    <script type="text/javascript">
        $(document).ready(function () {
			startTime = {{ $popup_start }} * 60000; // Convert minutes to milliseconds
			endTime = {{ $popup_end }} * 60000; // Convert minutes to milliseconds			
			
			setTimeout(function(){				   
               $('#modal-popup').modal('show');
            }, startTime);  
			
			setTimeout(function(){			
               $('#modal-popup').modal('hide');
            }, endTime);
			
        });				
    </script>
@endsection	