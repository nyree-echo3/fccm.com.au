<div class="col-lg-3 blog-sidebar">
    <div class="sidebar-module">
        <h4>{{ $category_name }}</h4>
        <ol class="navsidebar list-unstyled list-group-flush">
            @php
			   $counter = 0;
			@endphp  
						        
            @foreach($items as $item) 
                @php
				   $counter++;
				   
				   $href = request()->path();
				   				  
				@endphp  
                
                <li class='list-group-item'><a class="navsidebar lnkLink" id="lnkLink{{ $counter}}" href="#divLink{{ $counter}}" onclick="lnkLink{{$counter}}_onclick()">{{ $item->name }}</a></li> 
                
                <script>
					function lnkLink{{$counter}}_onclick()  {
						lnkLink = document.getElementsByClassName("lnkLink");						
						for(var i = 0; i < lnkLink.length; i++) {
						   lnkLink[i].style.backgroundColor = "#fff";
						}
												
						document.getElementById("lnkLink{{ $counter}}").style.backgroundColor = '#fff200';
					}
			    </script>                                                             
            @endforeach                        
        </ol>
    </div>
</div>
