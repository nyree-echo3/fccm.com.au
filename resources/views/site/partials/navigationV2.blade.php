<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top btco-hover-menu navbar-custom affix" data-toggle="affix">
    <div class="navbar-logo">
        <a href="{{ url('') }}/index" title="{{ $company_name }}"><img src="{{ url('') }}/images/site/logo.gif" title="{{ $company_name }}" alt="{{ $company_name }}"></a>
    </div>
    
	<button class="navbar-toggler custom-toggler hamburger hamburger--collapse hamburger--accessible js-hamburger" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
	  <span class="hamburger-box">
		<span class="hamburger-inner"></span>
	  </span>
	</button>
   
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <!--<li class="nav-item {{ (!isset($page_type) ? "active" : "") }}">
                <a class="nav-link" href="{{ url('') }}"><i class='fa fa-home'></i> <span
                            class="sr-only">(current)</span></a>
            </li>-->
            
            {!! $navigation !!}
            
            <li class="nav-item">
                <a class="nav-link" href="tel:{{ str_replace(' ', '', $phone_number) }}"><i class='fa fa-phone'></i> <span
                            class="sr-only">(current)</span></a>
            </li>
            
            <li class="nav-item">
                <a class="nav-link" href="{{ url('') }}/contact"><i class="far fa-envelope"></i> <span
                            class="sr-only">(current)</span></a>
            </li>
        </ul>                     
    </div>
</nav>


@section('inline-scripts-navigation')
    <script type="text/javascript">
		  var $hamburger = $(".hamburger");
		  $hamburger.on("click", function(e) {
			$hamburger.toggleClass("is-active");
			// Do something else, like open/close menu
		  });
    </script>
@endsection