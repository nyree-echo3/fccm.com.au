@php
if (!isset($header_image) || $header_image == "")  {
   $header_image = url('') . "/images/site/header-insidepage.jpg";
}

if (isset($page))  {
   $header_h1 = $page->title;
} else if (isset($category_name))  {
   $header_h1 = $category_name;
} else  {
   $header_h1 = "H1 Style Guide";
}
@endphp

<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner carousel-inside">
        <div class="carousel-item active">
            <img class="slide" src="{{ $header_image }}" alt="Header Image">
            <div class="container">
                <div class="carousel-caption text-left">
                   <h1 class="{{ (isset($news_item) ? "h1-news" : "")}}">{{ $header_h1 }}</h1>
                </div>
            </div>
        </div>
    </div>   
</div>