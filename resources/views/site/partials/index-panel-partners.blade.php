<div data-aos="fade-up" data-aos-duration="2000">
	<div class="index-panel-partners">
	   <div class="container-fluid">
		  <div class="row no-gutters h-100 align-items-center">         			 
			  <div class="col-lg-6">
			      <div id="partnersCarousel" class="carousel slide carousel-fade" data-ride="carousel"> 
		              @php 
					     $counter = 0 
					  @endphp
		          
			          @foreach($index_partners as $item)      
						  @php
						    $counter++
						  @endphp
	         
						  <div class="carousel-item {{ $counter == 1 ? ' active' : '' }}">	
						      <div class="index-panel-partners-txt">			 
								  <h2>A Partner’s Story</h2>
								  <p>{!! $item->description !!}</p>
								  <div class="index-panel-partners-btn"><a class="btn-submit" href="{{ url('partner-spotlight') }}">Read more</a></div>
							  </div>			 
						  </div>
					  @endforeach 
				  </div>
			  </div><!-- /.col-lg-6 -->	

			  <div class="col-lg-6">			 
				  <div class="index-panel-partners-img">
					 <img src="{{ url('') }}/images/site/pic2.png" alt="A Partner's Story" >
				  </div>						  			 
			  </div><!-- /.col-lg-6 -->				 	

			</div><!-- /.row -->	
	   </div><!-- /.container -->	
	</div><!-- /.index-panel-seo -->
</div>	