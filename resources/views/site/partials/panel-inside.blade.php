<div data-aos="fade-up" data-aos-duration="2000">
	<div class="panel-inside">
       <div class="panel-inside-shadow"></div>
       
	   <div class="container-fluid">
		  <div class="row no-gutters h-100 align-items-center">         			 			  

			 <div class="col-lg-6 mx-auto">
				  <div class="panel-inside-txt">			 
					  {!! $inside_intro_text !!}				 
				  </div>
			  </div><!-- /.col-lg-6 -->		

			</div><!-- /.row -->	
	   </div><!-- /.container -->	
	</div><!-- /.index-panel-seo -->	
</div>