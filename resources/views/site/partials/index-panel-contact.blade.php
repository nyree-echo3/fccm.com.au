@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/formvalidation/dist/css/formValidation.css') }}">
@endsection

<div data-aos="fade-up" data-aos-duration="2000">
	<div class="index-panel-contact">
       <div class="index-panel-contact-shadow"></div>
       
	   <div class="container-fluid">
		  <div class="row no-gutters h-100 align-items-center">         			 
			  <div class="col-lg-3"></div>
			  	
			  <div class="col-lg-6">			 
				  <div class="index-panel-contact-txt">
				     <h2>Get in touch with us</h2>
				     <p>Get in touch to discuss how FCCM can help improve your business and better deliver the sales results you’ve been searching for with your direct marketing. </p>
					 
					 <form id="contact-form" method="post" action="{{ url('contact/save-message') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div id="contact-form-fields"></div>

						<div class="form-row">
							<div class="col-12 g-recaptcha-container">
								<div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
								@if ($errors->has('g-recaptcha-response'))
									<div class="fv-help-block">{{ $errors->first('g-recaptcha-response') }}</div>
								@endif
							</div>
						</div>
						
					    <div class="index-panel-contact-btn">
 						   <button type="submit" class="btn-submit">Send</button>
						</div>						
					</form>
					
				  </div>						  			 
			  </div><!-- /.col-lg-6 -->					 

			</div><!-- /.row -->	
	   </div><!-- /.container -->	
	</div><!-- /.index-panel-contact -->	
</div>


@section('inline-scripts-contact')
    <script src="{{ asset('/components/formBuilder/dist/form-render.min.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/formValidation.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/plugins/Bootstrap.js') }}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#contact-form-fields').formRender({
                dataType: 'json',
                formData: {!! $form !!},
                notify: {
                    success: function(message) {

                        FormValidation.formValidation(
                            document.getElementById('contact-form'),
                            {
                                plugins: {
                                    declarative: new FormValidation.plugins.Declarative({
                                        html5Input: true,
                                    }),
                                    submitButton: new FormValidation.plugins.SubmitButton(),
                                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                                    bootstrap: new FormValidation.plugins.Bootstrap(),
                                    icon: new FormValidation.plugins.Icon({
                                        valid: 'fa fa-check',
                                        invalid: 'fa fa-times',
                                        validating: 'fa fa-refresh',
                                    })
                                },
                            }
                        );

                    }
                }
            });

        });
    </script>
@endsection