@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/popups') }}"><i class="fas fa-file-alt"></i> {{ $display_name }}</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/popups/update') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" value="{{ $popup->id }}">
                            <div class="box-body">
                                <div class="form-group{{ ($errors->has('title')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Title *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="title" name="title"
                                               placeholder="Title" value="{{ old('title',$popup->title) }}">
                                        @if ($errors->has('title'))
                                            <small class="help-block">{{ $errors->first('title') }}</small>
                                        @endif
                                    </div>
                                </div>
                                                               
                                <div class="form-group {{ ($errors->has('form')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Body *</label>

                                    <div class="col-sm-10">
                                        <textarea id="form" name="form" rows="10" cols="80"
                                                  style="height: 500px;">{{ old('body', $popup->form) }}</textarea>
                                        @if ($errors->has('form'))
                                            <small class="help-block">{{ $errors->first('form') }}</small>
                                        @endif
                                    </div>
                                </div>
                               
                                @php
                                    if(count($errors)>0){
                                       if(old('thumbnail')!=''){
                                        $thumbnail_image = old('thumbnail');
                                       }else{
                                        $thumbnail_image = '';
                                       }
                                    }else{
                                        $thumbnail_image = $popup->thumbnail;
                                    }
                                @endphp
                                <!--<div class="form-group {{ ($errors->has('thumbnail')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Thumbnail Image</label>
                                    <div class="col-sm-10">
                                        <input type="hidden" id="thumbnail" name="thumbnail"
                                               value="{{ old('thumbnail',$thumbnail_image) }}">
                                        <button id="image-popup" type="button" class="btn btn-info btn-sm">Upload
                                            Image
                                        </button>
                                        @php
                                            $class = ' invisible';
                                            if($thumbnail_image!=''){
                                                $class = '';
                                            }
                                        @endphp
                                        <button id="remove-image" type="button"
                                                class="btn btn-danger btn-sm{{ $class }}">Remove Image
                                        </button>
                                        <br/><br/>
                                        <span id="added_image">
                                        @if($thumbnail_image!='')
                                                <image src="{{ old('thumbnail',$thumbnail_image) }}"/>
                                            @endif
                                        </span>
                                        @if ($errors->has('thumbnail'))
                                            <small class="help-block">{{ $errors->first('thumbnail') }}</small>
                                        @endif
                                    </div>                                
                               </div>
                            
                            </div>-->
                                                       
                                                       
                             @php
                                if(count($errors)>0){
                                   if(old('live')=='on'){
                                    $status = 'active';
                                   }else{
                                    $status = '';
                                   }
                                }else{
                                    $status = $popup->status;
                                }
                            @endphp
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status *</label>
                                <div class="col-sm-10">
                                    <label>
                                        <input class="page_status" type="checkbox" data-toggle="toggle" data-size="mini"
                                               name="live" {{ $status == 'active' ? ' checked' : null }}>
                                    </label>
                                </div>
                            </div>                           
                                                                                          
                            <div class="box-footer">
                                <a href="{{ url('dreamcms/popups') }}" class="btn btn-info pull-right"
                                   data-toggle=confirmation data-title="Your changes will be lost! Are you sure?"
                                   data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                   data-btn-cancel-label="No">Cancel</a>                                                                   
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save_close">
                                    Save & Close
                                </button>                                                                
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save">Save
                                </button>                                                                    							
                            </div>
                            
                            <div class="box-footer">                                
                                                                                           
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div> 
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            CKEDITOR.replace('form');            

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $('input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });

            $('input[type="radio"].minimal').on('ifChecked', function (event) {

                if ($(this).val() == 'parent') {
                    $('#category_selector').show();
                    $('#page_selector').hide();
                }

                if ($(this).val() == 'child') {
                    $('#category_selector').hide();
                    $('#page_selector').show();
                }

            });
            
			$("#image-popup").click(function () {
                openPopup();
            });

            $("#remove-image").click(function () {
                $('#added_image').html('');
                $('#remove-image').addClass('invisible')
                $('#thumbnail').val('');
            });

        });
		
		function openPopup() {
            CKFinder.popup({
                chooseFiles: true,
                onInit: function (finder) {
                    finder.on('files:choose', function (evt) {
                        var file = evt.data.files.first();
                        $('#added_image').html('<image src="' + base_url + file.getUrl() + '">');
                        $('#remove-image').removeClass('invisible');
                        $('#thumbnail').val(file.getUrl());

                    });
                    finder.on('file:choose:resizedImage', function (evt) {
                        $('#added_image').html('<image src="' + base_url + evt.data.resizedUrl + '">');
                        $('#remove-image').removeClass('invisible');
                        $('#thumbnail').val(evt.data.resizedUrl);
                    });
                }
            });
        }
    </script>
@endsection