@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fas fa-file"></i> {{ $display_name }}</a></li>
            </ol>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-header">
                    <form method="post" action="{{ url('dreamcms/popups') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">                        
                    </form>

                    <div class="pull-right box-tools">                                               
                        <a href="{{ url('dreamcms/popups/add') }}" type="button" class="btn btn-info btn-sm"
                           data-widget="add">Add New
                            <i class="fa fa-plus"></i>
                        </a>                       
                    </div>
                </div>
                <div class="box-body">
                    @if(count($popups))
                        <table class="table table-hover">
                            <tr>
                                <th>@sortablelink('title')</th>                                
                                <th>@sortablelink('status')</th>
                                <th class="hd-text-right">Actions</th>
                            </tr>
                            @foreach($popups as $popup)
                                <tr>
                                    <td>{{ $popup->title }}</td>                                    
                                    <td>
                                        <input id="popup_{{ $popup->id }}" data-id="{{ $popup->id }}" class="faq_status" type="checkbox" data-toggle="toggle" data-size="mini"{{ $popup->status == 'active' ? ' checked' : null }}>
                                    </td>
                                    <td>
                                        <div class="pull-right">                                           
                                            <a href="{{ url('dreamcms/popups/'.$popup->id.'/edit') }}"
                                               class="tool"><i class="fa fa-edit"></i></a>                                            
                                            
                                            <a href="{{ url('dreamcms/popups/'.$popup->id.'/delete') }}"
                                               class="tool" data-toggle=confirmation data-title="Are you sure?"
                                               data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                               data-btn-cancel-label="No"><i class="far fa-trash-alt"></i></a>
                                        </div>
                                    </td>

                                </tr>
                            @endforeach
                        </table>
                    @else
                        No records
                    @endif
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-xs-6">
                            <form id="pagination_count_form" method="post" class="form-inline" action="{{ url('dreamcms/pagination') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <select id="pagination_count" name="pagination_count">
                                    <option value="25"{{ Session::get('pagination-count')==25 ? ' selected="selected"' : '' }}>25</option>
                                    <option value="50"{{ Session::get('pagination-count')==50 ? ' selected="selected"' : '' }}>50</option>
                                    <option value="100"{{ Session::get('pagination-count')==100 ? ' selected="selected"' : '' }}>100</option>
                                </select>
                                <span class="total-row"> Total {{ $popups->total() }} record</span>
                            </form>
                        </div>
                        <div class="col-xs-6" style="text-align: right;">
                            {{ $popups->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();

            $("#pagination_count").select2({
                minimumResultsForSearch: -1
            });

            $("#pagination_count").change(function() {
                $("#pagination_count_form").submit();
            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $('.faq_status').change(function() {
                $.ajax({
                    type: "POST",
                    url: "popups/"+$(this).data('id')+"/change-popup-status",
                    data:  {
                        'status':$(this).prop('checked')
                    },
                    success: function (response) {
                        if(response.status=="success"){
                            toastr.options = {"closeButton": true}
                            toastr.success('Status has been changed');
                        }
                    }
                });
            });

        });
    </script>
@endsection