@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/font-awesome-old/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/iCheck/all.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-envelope"></i> {{ $display_name }}</a></li>
                <li class="active">Inbox</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12">

                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Inbox</h3>

                            <form id="message-search-form" method="post" class="form-inline" action="{{ url('dreamcms/contact') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="box-tools pull-right">
                                    <div class="has-feedback">
                                        <input type="text" name="search" class="form-control input-sm" placeholder="Search Message" value="{{ (isset($session['search']) ? $session['search'] : "") }}">
                                        <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                    </div>
                                </div>
                            </form>

                        </div>

                        <div class="box-body no-padding">
                            <div class="mailbox-controls">

                                <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
                                </button>
                                @can('delete-message')
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default btn-sm" data-toggle=confirmation data-title="Are you sure?" data-popout="true" data-singleton="true" data-btn-ok-label="Yes" data-btn-cancel-label="No"><i class="fa fa-trash-o"></i></button>
                                    </div>
                                @endcan
                                <button type="button" class="btn btn-default btn-sm refresh-messages"><i class="fa fa-refresh"></i></button>

                            </div>

                            <form id="message-delete-form" method="post" class="form-inline" action="{{ url('dreamcms/contact/delete') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="table-responsive mailbox-messages">
                                    @if(count($messages))
                                        <table class="table table-hover table-striped">
                                            <tbody>
                                            @foreach($messages as $message)
                                                @php
                                                $properties = json_decode($message->data);
                                                $name = '';
                                                $message_text = '';

                                                foreach($properties as $property){
                                                    if($property->field=='name'){
                                                        $name = $property->value;
                                                    }
                                                    if($property->field=='message'){
                                                        $message_text = $property->value;
                                                    }
                                                }
                                                @endphp
                                                @if($message->status=="unread")
                                                    <tr style="background-color: #e2e2e2">
                                                @else
                                                    <tr>
                                                        @endif
                                                        <td style="width: 40px;"><input type="checkbox" name="messages[]" value="{{ $message->id }}"></td>
                                                        <td class="mailbox-name">
                                                            @can('read-message')
                                                                <a href="{{ url('dreamcms/contact/'.$message->id.'/read') }}">{{ $name }}</a>
                                                            @else
                                                                {{ $name }}
                                                            @endcan
                                                        </td>
                                                        <td class="mailbox-subject">{{ $message->type }}</td>
                                                        <td class="mailbox-date">{{ $message->created_at->diffForHumans() }}</td>
                                                    </tr>
                                                    @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        <div class="col-xs-12">No record</div>
                                    @endif
                                </div>
                            </form>

                        </div>

                        <div class="box-footer no-padding">

                            <div class="col-xs-6 mailbox-controls">

                                <div class="mailbox-controls-container">

                                    <div class="tool-buttons">
                                        <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></button>

                                        @can('delete-message')
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default btn-sm" data-toggle=confirmation data-title="Are you sure?" data-popout="true" data-singleton="true" data-btn-ok-label="Yes" data-btn-cancel-label="No"><i class="fa fa-trash-o"></i></button>
                                            </div>
                                        @endcan
                                        <button type="button" class="btn btn-default btn-sm refresh-messages"><i class="fa fa-refresh"></i></button>
                                    </div>

                                    <div class="paginate-dropdown">
                                        <form id="pagination_count_form" method="post" class="form-inline" action="{{ url('dreamcms/pagination') }}">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <select id="pagination_count" name="pagination_count">
                                                <option value="25"{{ Session::get('pagination-count')==25 ? ' selected="selected"' : '' }}>25</option>
                                                <option value="50"{{ Session::get('pagination-count')==50 ? ' selected="selected"' : '' }}>50</option>
                                                <option value="100"{{ Session::get('pagination-count')==100 ? ' selected="selected"' : '' }}>100</option>
                                            </select>
                                            <span class="total-row"> Total {{ $messages->total() }} messages</span>
                                        </form>
                                    </div>

                                </div>

                            </div>

                            <div class="col-xs-6" style="text-align: right;">
                                {{ $messages->links() }}
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </section>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
    <script src="{{ asset('/components/theme/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('/components/theme/plugins/fastclick/fastclick.min.js') }}"></script>
    <script src="{{ asset('/components/theme/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('/js/admin/inbox.js') }}"></script>
@endsection