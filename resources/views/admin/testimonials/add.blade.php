@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/testimonials') }}"><i class="far fa-comments"></i> {{ $display_name }}</a></li>
                <li class="active">Add New</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add New</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/testimonials/store') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="box-body">
                                <div class="form-group{{ ($errors->has('person')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Person/Company *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="person" placeholder="Person/Company"
                                               value="{{ old('person') }}">
                                        @if ($errors->has('person'))
                                            <small class="help-block">{{ $errors->first('person') }}</small>
                                        @endif
                                    </div>
                                </div>                                

                                <div class="form-group {{ ($errors->has('description')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Description *</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" rows="3" name="description"
                                                  placeholder="Description">{{ old('description') }}</textarea>
                                        @if ($errors->has('description'))
                                            <small class="help-block">{{ $errors->first('description') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group {{ ($errors->has('image')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Image</label>
                                    <div class="col-sm-10">
                                        <input type="hidden" id="image" name="image"
                                               value="{{ old('image') }}">
                                        <button id="image-popup" type="button" class="btn btn-info btn-sm">Upload
                                            Image
                                        </button>
                                        @php
                                            $class = ' invisible';
                                            if(old('image')){
                                                $class = '';
                                            }
                                        @endphp
                                        <button id="remove-image" type="button"
                                                class="btn btn-danger btn-sm{{ $class }}">Remove Image
                                        </button>
                                        <br/><br/>
                                        <span id="added_image">
                                        @if(old('image'))
                                                <image src="{{ old('image') }}"/>
                                            @endif
                                        </span>
                                        @if ($errors->has('image'))
                                            <small class="help-block">{{ $errors->first('image') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                @php
                                    $status = 'active';
                                    if(count($errors)>0){
                                       if(old('live')=='on'){
                                        $status = 'active';
                                       }else{
                                        $status = '';
                                       }
                                    }
                                @endphp
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Status *</label>
                                    <div class="col-sm-10">
                                        <label>
                                            <input class="page_status" type="checkbox" data-toggle="toggle"
                                                   data-size="mini"
                                                   name="live" {{ $status == 'active' ? ' checked' : null }}>
                                        </label>
                                    </div>
                                </div>

                                <div class="box-footer">
                                    <a href="{{ url('dreamcms/testimonials') }}" class="btn btn-info pull-right"
                                       data-toggle=confirmation data-title="Your changes will be lost! Are you sure?"
                                       data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                       data-btn-cancel-label="No">Cancel</a>
                                    <button type="submit" class="btn btn-info pull-right" name="action"
                                            value="save_close">Save & Close
                                    </button>
                                    <button type="submit" class="btn btn-info pull-right" name="action" value="save">
                                        Save
                                    </button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();

            CKEDITOR.replace('description');

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
			
			$("#image-popup").click(function () {
                openPopup();
            });

            $("#remove-image").click(function () {
                $('#added_image').html('');
                $('#remove-image').addClass('invisible')
                $('#image').val('');
            });

        });
		
		function openPopup() {
            CKFinder.popup({
                resourceType:'Images',
                chooseFiles: true,
                onInit: function (finder) {
                    finder.on('files:choose', function (evt) {
                        var file = evt.data.files.first();
                        $('#added_image').html('<image src="' + base_url + file.getUrl() + '">');
                        $('#remove-image').removeClass('invisible');
                        $('#image').val(file.getUrl());

                    });
                    finder.on('file:choose:resizedImage', function (evt) {
                        $('#added_image').html('<image src="' + base_url + evt.data.resizedUrl + '">');
                        $('#remove-image').removeClass('invisible');
                        $('#image').val(evt.data.resizedUrl);
                    });
                }
            });
        }
    </script>
@endsection