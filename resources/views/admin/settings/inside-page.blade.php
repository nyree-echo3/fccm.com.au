@extends('admin.layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/iCheck/all.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Inside Page</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Inside Page</a></li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Inside Page Values</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/settings/inside-page-update') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="box-body">
                               
                                <div class="form-group {{ ($errors->has('inside_intro_text')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Inside Text</label>

                                    <div class="col-sm-10">
                                        <textarea id="inside_intro_text" name="inside_intro_text" rows="10" cols="80"
                                                  style="height: 500px;">{{ $inside_intro_text->value }}</textarea>
                                        @if ($errors->has('inside_intro_text'))
                                            <small class="help-block">{{ $errors->first('inside_intro_text') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/iCheck/icheck.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            CKEDITOR.replace('inside_intro_text');
        });
    </script>
@endsection