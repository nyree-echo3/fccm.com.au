$( document ).ready(function() {

    var $fotoramaDiv = $('.fotorama').fotorama();
    var fotorama = $fotoramaDiv.data('fotorama');

    $( "#floorplan" ).click(function() {
        $('.fotorama__stage').append('<div class="fotorama-floorplan"><img class="floorplan-img" src="'+$(this).data('href')+'"></div>');
    });

    $('.fotorama').on('fotorama:show', function (e, fotorama) {

        if($('.fotorama-floorplan').length){
            $('.fotorama-floorplan').remove();
        }
    });
});