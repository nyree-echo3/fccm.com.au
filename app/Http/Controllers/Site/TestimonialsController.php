<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Testimonial;

class TestimonialsController extends Controller
{
    public function index($mode = ""){				
	    $items = $this->getItems($mode);		
			
		return view('site/testimonials/list', array(         					
			'items' => $items,	
			'mode' => $mode,
        ));

    }
		
	public function getItems($mode){
		if ($mode == "preview") {
		   $items = Testimonial::orderBy('position', 'asc')->get();						
		} else {
		   $items = Testimonial::where('status', '=', 'active')->orderBy('position', 'asc')->get();						
		}
		return($items);
	}		
}
