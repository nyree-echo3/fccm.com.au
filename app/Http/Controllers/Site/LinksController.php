<?php

namespace App\Http\Controllers\Site;

use App\Helpers\NavigationBuilder;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Link;
use App\LinkCategory;

class LinksController extends Controller
{
    public function list($category_slug = "", $item_slug = ""){

        $side_navV2 = (new NavigationBuilder())->buildSideNavigation();

        $side_nav_mode = 'manual';
        if($side_navV2==null){
            $side_navV2 = $this->getCategories();
            $side_nav_mode = 'auto';
        }

		if (sizeof($this->getCategories()) > 0)  {
			if ($category_slug == "")  {
			   // Get Latest Links
			   $category_name = "Links";	

			   $items = $this->getLinks();
			} elseif ($category_slug != "" && $item_slug == "") {
			  // Get Category Links	
			  $category = $this->getCategory($category_slug);
			  $category_name = $category->name;	

			  $items = $this->getLinks($category->id);			  
			} 		
		}

		return view('site/links/list', array(
            'side_nav' => $side_navV2,
            'side_nav_mode' => $side_nav_mode,
			'category_name' => (sizeof($this->getCategories()) > 0 ? $category_name : null),
			'items' => (sizeof($this->getCategories()) > 0 ? $items : null),
        ));

    }
	 	
	public function getCategories(){
		$categories = LinkCategory::where('status', '=', 'active')->get();
        foreach ($categories as $category){
            $category->url = $category->url;
        }
		return($categories);
	}	
	
	public function getLinks($category_id = "", $limit = 2){
		if ($category_id == "")  {
			$links = Link::where('status', '=', 'active')						
						->orderBy('position', 'desc')
						->paginate($limit);	
		} else {
		   $links = Link::where('status', '=', 'active')
			            ->where('category_id', '=', $category_id)						
						->orderBy('position', 'desc')
						->paginate($limit);		
		}
		
		return($links);
	}	
	  
	public function getLinkItem($item_slug){		
		$link = Link::where(['status' => 'active', 'slug' => $item_slug])
			        ->where('status', '=', 'active')
			        ->first();						
		return($link);
	}
	
	public function getCategory($category_slug){
		$categories = LinkCategory::where('slug', '=', $category_slug)->first();		
		return($categories);
	}		
}
