<?php

namespace App\Http\Controllers\Site;

use App\ContactMessages;
use App\Http\Controllers\Controller;
use App\Mail\ContactMessageAdmin;
use App\Mail\ContactMessageUser;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator, Illuminate\Support\Facades\Input, Redirect;

class ContactController extends Controller
{
    public function index(){

        $fields = Setting::where('key', '=', 'contact-form-fields')->first();

        // Contact Details
        $contact_details_top = Setting::where('key', '=', 'contact-details-top')->first();

        // Contact Details TOP
        $contact_details = Setting::where('key', '=', 'contact-details')->first();
		
		// Contact Map
        $contact_map = Setting::where('key', '=', 'contact-map')->first();

        $form = $fields->value;

        if(old('name')){
            $fields = json_decode($fields->value);
            foreach ($fields as $field){
                $field->value = old($field->name);
            }

            $form = json_encode($fields);
        }

        return view('site/contact/contact', array(
            'form' => $form,
            'contact_details_top' => $contact_details_top->value,
            'contact_details' => $contact_details->value,
			'contact_map' => $contact_map->value,
			'category_name' => "Contact"
        ));
    }

    public function saveMessage(Request $request)
    {

        $rules = array(
            'g-recaptcha-response' => 'required|recaptcha'
        );

        $messages = [
            'g-recaptcha-response.required' => 'Please verify yourself',
            'g-recaptcha-response.recaptcha' => 'Please verify yourself'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('contact')->withErrors($validator)->withInput();
        }

        $fields = Setting::where('key', '=', 'contact-form-fields')->first();
        $fields = json_decode($fields->value);

        $data = array();

        foreach ($fields as $field){

            if($field->type!='header' && $field->type!='paragraph'){

                if($field->type=='date'){
                    $date_array = explode('-',$request->{$field->name});
                     $request->{$field->name} = $date_array[2].'-'.$date_array[1].'-'.$date_array[0];
                }

                $data_field['label'] = $field->label;
                $data_field['field'] = $field->name;
                $data_field['value'] = $request->{$field->name};
                array_push($data, $data_field);
            }
        }

        $message = new ContactMessages();
        $message->data = json_encode($data);
        $message->type = "Contact Form";
        $message->save();

		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
		// Email Website Owner
        Mail::to($contactEmail)->send(new ContactMessageAdmin($message));
		
		// Email User
        Mail::to($request->email)->send(new ContactMessageUser($message));

        return \Redirect::to('contact/success');
    }

    public function success(){
        // Contact Details
        $contact_details = Setting::where('key', '=', 'contact-details')->first();
		
		// Contact Map
        $contact_map = Setting::where('key', '=', 'contact-map')->first();
		
        return view('site/contact/success', array(            
            'contact_details' => $contact_details->value,
			'contact_map' => $contact_map->value,
			'category_name' => "Contact"
        ));
    }
	
	public function saveMessageGolive(Request $request)
    {
        $settings = Setting::where('key', '=', 'company-name')->first();
        $company_name = $settings->value;        

        $message = new ContactMessages();		
        $message->data = '[{"label":"Type","field":"type","value":"Go Live Notification"}, {"label":"Name","field":"name","value":"' . $request->email . '"}, {"label":"Email","field":"email","value":"' . $request->email . '"}]';		
        $message->status = "unread";        
        $message->save();
		
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
		// Email Website Owner
        Mail::to($contactEmail)->send(new ContactMessageAdmin($message));
		
		// Email User
        Mail::to($request->email)->send(new ContactMessageUser($message));

       return \Redirect::to('');
    }
	
	public function newsletter(){       

        return view('site/contact/newsletter', array(
			'category_name' => "Subscribe to our news"
        ));
    }

    public function saveNewsletter(Request $request)
    {

        $rules = array(
            'g-recaptcha-response' => 'required|recaptcha'
        );

        $messages = [
            'g-recaptcha-response.required' => 'Please verify yourself',
            'g-recaptcha-response.recaptcha' => 'Please verify yourself'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('newsletter')->withErrors($validator)->withInput();
        }

        $fields = Setting::where('key', '=', 'contact-form-fields')->first();
        $fields = json_decode($fields->value);

        $data = array();

        foreach ($fields as $field){

            if($field->type!='header' && $field->type!='paragraph'){

                if($field->type=='date'){
                    $date_array = explode('-',$request->{$field->name});
                    $request->{$field->name} = $date_array[2].'-'.$date_array[1].'-'.$date_array[0];
                }

                $data_field['label'] = $field->label;
                $data_field['field'] = $field->name;
                $data_field['value'] = $request->{$field->name};
                array_push($data, $data_field);
            }
        }

        $message = new ContactMessages();
        $message->data = json_encode($data);
        $message->type = "Newsletter";
        $message->save();

        $setting = Setting::where('key','=','contact-email')->first();
        $contactEmail = $setting->value;

        // Email Website Owner
        Mail::to($contactEmail)->send(new ContactMessageAdmin($message));

        // Email User
        Mail::to($request->email)->send(new ContactMessageUser($message));

        return \Redirect::to('newsletter/success');
    }

    public function successNewsletter(){
        // Contact Details
        $contact_details = Setting::where('key', '=', 'contact-details')->first();

        // Contact Map
        $contact_map = Setting::where('key', '=', 'contact-map')->first();

        return view('site/contact/newsletter-success', array(
            'contact_details' => $contact_details->value,
            'contact_map' => $contact_map->value,
            'category_name' => "Subscribe to our news"
        ));
    }

    public function leadMagnet(){

        return view('site/contact/lead-magnet', array(
            'category_name' => "Supercharge Your Direct Marketing"
        ));
    }

    public function saveLeadMagnet(Request $request)
    {

        $rules = array(
            'g-recaptcha-response' => 'required|recaptcha'
        );

        $messages = [
            'g-recaptcha-response.required' => 'Please verify yourself',
            'g-recaptcha-response.recaptcha' => 'Please verify yourself'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('steps-to-supercharge-your-direct-marketing')->withErrors($validator)->withInput();
        }

        $fields = Setting::where('key', '=', 'contact-form-fields')->first();
        $fields = json_decode($fields->value);

        $data = array();

        foreach ($fields as $field){

            if($field->type!='header' && $field->type!='paragraph'){

                if($field->type=='date'){
                    $date_array = explode('-',$request->{$field->name});
                    $request->{$field->name} = $date_array[2].'-'.$date_array[1].'-'.$date_array[0];
                }

                $data_field['label'] = $field->label;
                $data_field['field'] = $field->name;
                $data_field['value'] = $request->{$field->name};
                array_push($data, $data_field);
            }
        }

        $message = new ContactMessages();
        $message->data = json_encode($data);
        $message->type = "Lead Magnet";
        $message->save();

        $setting = Setting::where('key','=','contact-email')->first();
        $contactEmail = $setting->value;

        // Email Website Owner
        Mail::to($contactEmail)->send(new ContactMessageAdmin($message));

        // Email User
        Mail::to($request->email)->send(new ContactMessageUser($message));

        return \Redirect::to('steps-to-supercharge-your-direct-marketing/success');
    }

    public function successLeadMagnet(){
        // Contact Details
        $contact_details = Setting::where('key', '=', 'contact-details')->first();

        // Contact Map
        $contact_map = Setting::where('key', '=', 'contact-map')->first();

        return view('site/contact/lead-magnet-success', array(
            'contact_details' => $contact_details->value,
            'contact_map' => $contact_map->value,
            'category_name' => "Supercharge Your Direct Marketing"
        ));
    }
}
