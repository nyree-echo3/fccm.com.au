<?php

namespace App\Http\Controllers\Site;

use App\Helpers\NavigationBuilder;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Mail;
use Eway\Rapid;

use App\Setting;
use App\Member;
use App\MemberType;
use App\Mail\MembersMessageAdmin;
use App\Mail\MembersMessageUser;


class MembersController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:member');
    }
	
	 public function index()
    {
        // Company Name
        $company_name = Setting::where('key', '=', 'company-name')->first();
		$company_name = $company_name->value;
		         

        return view('site/members/home', array(
            'company_name' => $company_name,           			
        ));
    }	
	
	public function changeDetails()
    {
		// Company Name
        $company_name = Setting::where('key', '=', 'company-name')->first();
		$company_name = $company_name->value;
		        		
		$member = Auth::guard('member')->user();		
		
        return view('site/members/change-details', array(
			'company_name' => $company_name,        
            'member' => $member,           
        ));
    }
	
	public function saveDetails (Request $request)
    {				
		// Logged in Member
		$member = Auth::guard('member')->user();
		
		// Field Validation
	    $rules = array(                                  
			'firstName' => 'required',
			'lastName' => 'required',
			'phoneNumber' => 'required',
			'email' => 'required|string|email|max:255|unique_update:members,'.$member->id,
			'address1' => 'required',
			'suburb' => 'required',
			'state' => 'required',
			'postcode' => 'required',			
        );

        $messages = [           			
			'firstName.required' => 'Please enter first name',
			'lastName.required' => 'Please enter last name',
			'phoneNumber.required' => 'Please enter phone number',
			'email.required' => 'Please enter email',
			'email.unique_store' => 'We already have a member account using the provided email address',
			'address1.required' => 'Please enter address line 1',
			'suburb.required' => 'Please enter suburb',
			'state.required' => 'Please enter state',
			'postcode.required' => 'Please enter postcode',					
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
			flash("<span class='alert-error'><i class='fas fa-exclamation'></i> There was a problem saving your details.  Please try again.</span>");	
            return redirect('members-portal/change-details')->withErrors($validator)->withInput();
        }
					                		
        $member->firstName = $request->firstName;
		$member->lastName = $request->lastName;		
		$member->address1 = $request->address1;
		$member->address2 = $request->address2;
		$member->suburb = $request->suburb;
		$member->state = $request->state;
		$member->postcode = $request->postcode;
		$member->phoneMobile = $request->phoneNumber;		
		$member->email = $request->email;		
        $member->save();								 
		
		flash("<span class='alert-success'><i class='fas fa-check'></i> Your details were saved successfully.</span>");	
  	    return \Redirect::to('members-portal/change-details');
	}
	
	public function changePassword()
    {
		// Company Name
        $company_name = Setting::where('key', '=', 'company-name')->first();
		$company_name = $company_name->value;		        				
		
        return view('site/members/change-password', array(
			'company_name' => $company_name,                    
        ));
    }
	
	public function savePassword (Request $request)
    {				
		// Logged in Member
		$member = Auth::guard('member')->user();
		
		if (!(Hash::check($request->get('passwordCurrent'), $member->password))) {           
			flash("<span class='alert-error'><i class='fas fa-exclamation'></i> Your current password does not matches with the password you provided. Please try again.</span>");	
            return redirect()->back()->withInput();
        }
		
		if(strcmp($request->get('passwordCurrent'), $request->get('password')) == 0){
            flash("<span class='alert-error'><i class='fas fa-exclamation'></i> The new password cannot be same as your current password. Please choose a different password.</span>");	
            return redirect()->back()->withInput();
        }
				
        $member->password = Hash::make($request->get('password'));
        $member->save();								 
		
		flash("<span class='alert-success'><i class='fas fa-check'></i> Your password was saved successfully.</span>");	
  	    return \Redirect::to('members-portal/change-password');
	
	}
}
