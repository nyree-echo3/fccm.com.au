<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Rutorika\Sortable\SortableTrait;
//use Laravel\Scout\Searchable;

class Link extends Model
{
    use SortableTrait, Sortable;//, Searchable;

    protected $table = 'links';

    public $sortable = ['title', 'category_id', 'status'];

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function toSearchableArray()
    {
        $array = $this->toArray();
        return $array;
    }

    public function category()
    {
        return $this->belongsTo(LinkCategory::class, 'category_id');
    }

    public function categorysort()
    {
        return $this->hasOne(LinkCategory::class,'id','category_id');
    }   

    public function scopeFilter($query)
    {

        $filter = session()->get('links-filter');
        $select = "";

        if($filter['category'] && $filter['category']!="all"){
            $select =  $query->where('category_id', $filter['category']);
        }

        if($filter['search']){
            $select =  $query->where('title','like', '%'.$filter['search'].'%');
        }

        return $select;
    }
    
}
