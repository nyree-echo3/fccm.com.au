<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LinkCategory extends Model
{
    protected $table = 'link_categories';

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function links()
    {
        return $this->hasMany(Link::class, 'category_id')->where('status', '=', 'active')->orderBy('position', 'desc');
    }
	
	public function getUrlAttribute()
    {
        $special_url = SpecialUrl::where('item_id','=', $this->attributes['id'])->where('module','=','links')->where('type','=','category')->first();
        if($special_url){
            return $special_url->url;
        }

        return 'links/'.$this->attributes['slug'];
    }
		
}
