<?php

namespace App\Mail;

use App\Donation;
use App\Setting;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DonationMessageUser extends Mailable
{
    use Queueable, SerializesModels;

    public $donation_message;

    public function __construct(Donation $donation_message)
    {
        $this->donation_message = $donation_message;
    }

    public function build()
    {
		$setting = Setting::where('key','=','company-name')->first();
		$companyName = $setting->value;
		
		$setting = Setting::where('key','=','contact-details')->first();
		$contactDetails = $setting->value;
		
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
        return $this->subject($companyName)
			        ->from($contactEmail)
			        ->view('site/emails/donation-message-user', array(
						'companyName' => $companyName, 
						'contactDetails' => $contactDetails, 
					));
    }
}
