<?php

namespace App\Mail;

use App\ContactMessages;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Setting;

class ContactMessageAdmin extends Mailable
{
    use Queueable, SerializesModels;

    public $contact_message;

    public function __construct(ContactMessages $contact_message)
    {
        $this->contact_message = $contact_message;
    }

    public function build()
    {
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
        return $this->subject('Website | ' . $this->contact_message->type)
			        ->from($contactEmail)
			        ->view('site/emails/contact-message-admin');
    }
}
