<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
		DB::table('states')->insert([           
            'state' => 'VIC'          
        ]);
		DB::table('states')->insert([           
            'state' => 'NSW'          
        ]);
		DB::table('states')->insert([           
            'state' => 'QLD'          
        ]);
		DB::table('states')->insert([           
            'state' => 'ACT'          
        ]);
		DB::table('states')->insert([           
            'state' => 'NT'          
        ]);
		DB::table('states')->insert([           
            'state' => 'WA'          
        ]);
		DB::table('states')->insert([           
            'state' => 'SA'          
        ]);
		DB::table('states')->insert([           
            'state' => 'TAS'          
        ]);
    }
}
