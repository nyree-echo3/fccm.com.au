<?php 
// Home Page
Route::get('/', 'HomeController@index');
Route::get('/index', 'HomeController@indexDev');  // To bypass holding page in development mode

// Content Styles
Route::get('/style-guide', 'HomeController@styleGuide');

// Contact Form
Route::group(['prefix' => '/contact'], function () {
    Route::get('/', 'ContactController@index');
    Route::post('save-message', 'ContactController@saveMessage');
    Route::get('success', 'ContactController@success');
	Route::post('save-message-golive', 'ContactController@saveMessageGolive');
});

// Newsletter Form
Route::group(['prefix' => '/newsletter'], function () {
    Route::get('/', 'ContactController@newsletter');
    Route::post('save-message', 'ContactController@saveNewsletter');
    Route::get('success', 'ContactController@successNewsletter');
});

// Lead Magnet
Route::group(['prefix' => '/steps-to-supercharge-your-direct-marketing'], function () {
    Route::get('/', 'ContactController@leadMagnet');
    Route::post('save-message', 'ContactController@saveLeadMagnet');
    Route::get('success', 'ContactController@successLeadMagnet');
});

// Pages Module
Route::group(['prefix' => '/pages'], function () {
    Route::get('{category}', 'PagesController@index');    
	Route::get('{category}/{page}', 'PagesController@index');    
});

// News Module
Route::group(['prefix' => '/news'], function () {
	Route::get('', 'NewsController@list'); 	
	
	Route::get('archive', 'NewsController@archive'); 
	Route::get('archive/{age}', 'NewsController@archive'); 
	
    Route::get('{category}', 'NewsController@list');    
	Route::get('{category}/{page}', 'NewsController@item'); 
	
});

// Faqs Module
Route::group(['prefix' => '/faqs'], function () {
	Route::get('', 'FaqsController@index'); 
    Route::get('{category}', 'FaqsController@index');    	
});

// Documents Module
Route::group(['prefix' => '/documents'], function () {
	Route::get('', 'DocumentsController@index'); 
    Route::get('{category}', 'DocumentsController@index');    	
});

// Gallery Module
Route::group(['prefix' => '/gallery'], function () {
	Route::get('', 'GalleryController@index'); 
    Route::get('{category}', 'GalleryController@detail');    	
});

// Projects Module
Route::group(['prefix' => '/projects'], function () {
	Route::get('', 'ProjectsController@list'); 			
	
    Route::get('{category}', 'ProjectsController@list');    
	Route::get('{category}/{page}', 'ProjectsController@item');
});

// Links Module
Route::group(['prefix' => '/links'], function () {
	Route::get('', 'LinksController@list'); 				
    Route::get('{category}', 'LinksController@list');    	;
});

// Properties Module
Route::group(['prefix' => '/properties'], function () {
    Route::get('{category}', 'PropertiesController@list');
    Route::get('{category}/{page}', 'PropertiesController@item');
});


// Team Module
Route::group(['prefix' => '/team'], function () {
    Route::get('', 'TeamController@index');
    Route::get('{category}', 'TeamController@index');
    Route::get('{category}/{member}', 'TeamController@member');
});

// Donation Module
Route::group(['prefix' => '/donation'], function () {
    Route::get('/', 'DonationsController@index');
    Route::post('save-donation', 'DonationsController@saveDonation');
    Route::get('success/{donation}', 'DonationsController@success');
	Route::get('error/{donation}', 'DonationsController@success');
});

// ***********
// Members Module
// ***********
// Register
Route::group(['prefix' => '/register'], function () {    
	Route::get('/', 'Auth\RegisterController@register');	
    Route::post('store', 'Auth\RegisterController@store');
    Route::get('success/{member}', 'Auth\RegisterController@success');			
}); 

// Login
Route::group(['prefix' => '/login'], function () {
    Route::get('/', 'Auth\LoginController@showLoginForm');
	Route::post('/', 'Auth\LoginController@login');
	
	
   // Route::post('process', 'MembersController@process');
	
	//Route::get('forgot', 'MembersController@forgot');
}); 

// Logout
Route::group(['prefix' => '/logout'], function () {
    Route::get('/', 'Auth\LoginController@logout');	
}); 

// Password Reset
Route::group(['prefix' => '/password'], function () {
    Route::get('forgot', 'Auth\ForgotPasswordController@showLinkRequestForm');	
	Route::post('email', 'Auth\ForgotPasswordController@sendResetLinkEmail');	
	
	Route::get('reset/{token}', 'Auth\ResetPasswordController@showResetForm');	
	Route::post('reset/', 'Auth\ResetPasswordController@reset');	
}); 

// Members Only Section
Route::group(['prefix' => '/members-portal'], function () {
    Route::get('/', 'MembersController@index');
	
	Route::get('change-details', 'MembersController@changeDetails');
	Route::post('save-details', 'MembersController@saveDetails');
	
	Route::get('change-password', 'MembersController@changePassword');
	Route::post('save-password', 'MembersController@savePassword');
}); 


// ***********
// Shop Module
// ***********
Route::group(['prefix' => '/products', 'as' => 'products.'], function() {
    Route::get('', 'ProductsController@list')->name('list'); 	
	
	Route::get('{category}', 'ProductsController@list');    
	Route::get('{category}/{page}', 'ProductsController@item'); 	
});

Route::group(['prefix' => 'cart', 'as' => 'cart.'], function() {
    Route::get('show', 'CartController@show')->name('show');
    Route::post('add/{product}', 'CartController@add')->name('add');
    Route::post('remove/{cart_item}', 'CartController@remove')->name('remove');
});

Route::group(['prefix' => 'checkout', 'as' => 'checkout.'], function() {
    Route::get('show', 'CheckoutController@show')->name('show');	
    Route::post('submit', 'CheckoutController@submit')->name('submit');
		
	Route::get('complete/{order}', 'CheckoutController@complete')->name('complete');
	Route::get('cancel/{order}', 'CheckoutController@cancel')->name('cancel');
});
// ***********

// Testimonials Module
Route::group(['prefix' => '/testimonials'], function () {
    Route::get('', 'TestimonialsController@index');
});